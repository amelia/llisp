
local green = "\27[32m"
local red = "\27[31m"
local reset = "\27[0m"


binary = "./a.out"
tests = {
	{
		file = "./test/add.ll",
		ex_return = "60",
		ex_out = "",
	},{
		file = "./test/def.ll",
		ex_return = "5",
		ex_out = "",
	},{
		file = "./test/exit.ll",
		ex_return = "5",
		ex_out = "no aaa",
	},{
		file = "./test/if.ll",
		ex_return = "0",
		ex_out = "test\nno",
	},{
		file = "./test/mul.ll",
		ex_return = "110",
		ex_out = "",
	},{
		file = "./test/nested.ll",
		ex_return = "5",
		ex_out = "meow\nnya\nuwu",
	},{
		file = "./test/redef.ll",
		ex_return = "20",
		ex_out = "",
	},{
		file = "./test/sub.ll",
		ex_return = "10",
		ex_out = "",
	},{
		file = "./test/arth.ll",
		ex_return = "203",
		ex_out = "",
	},{
		file = "./test/outint.ll",
		ex_return = "0",
		ex_out = "36",
	},{
		file = "./test/array.ll",
		ex_return = "4",
		ex_out = "6\n5\n2222\n5",
	},{
		file = "./test/assoc.ll",
		ex_return = "2",
		ex_out = "wowa wowa",
	},{
		file = "./test/fibonacci.ll",
		ex_return = "0",
		ex_out = "1\n2\n3\n5\n8\n13\n21\n34\n55\n89\nend",
	},{
		file = "./test/defn.ll",
		ex_return = "0",
		ex_out = "wowa",
	},{
		file = "./test/types.ll",
		ex_return = "5",
		ex_out = "4\n1\n8\n13\n0",
	},{
		file = "./test/rec.ll",
		ex_return = "0",
		ex_out = "5\n4\n3\n2\n1",
	},{
		file = "./test/fibonacci2.ll",
		ex_return = "0",
		ex_out = "34",
	},
}

function c_exit(i)
	print(i)
	if(type(i)==type(true)) then
		return i and 1 or 0
	end
	return i
end
function run_test(test)
	local r = io.popen(binary.." "..test.file.."; echo -n $?")
	local output = r:read("*all")
	local exit
	for l in output:gmatch("[^\n]+") do
		exit = l
	end
	local len = 2;
	for l in exit:gmatch(".") do
		len = len + 1	
	end
	output = output:sub(1,-len)
	if(output==test.ex_out) then
		if(exit==test.ex_return) then
			return true
		end
		print("\n\nwrong exit code "..exit.."!="..test.ex_return)
		return false
	end
	print("\nwrong output '"..output.."'!='"..test.ex_out.."'")
	return false
end

local pass = 0 
local fail = 0
local filest = {}

local dir = io.popen("ls test")
local test_count = 0
local rtest_count = 0
for t in dir:read("*all"):gmatch("[^\n]+") do
	table.insert(filest,"./test/"..t)
	test_count = test_count + 1
end
function rfromt(tab,target)
	for i,t in pairs(tab) do
		if(t==target) then
			table.remove(tab,i)
			return tab
		end
	end
	return tab
end
for ind,t in pairs(tests) do	
	--table.remove(filest,ind);
	filest = rfromt(filest,t.file)
	rtest_count = rtest_count + 1
	io.write("--- *** "..t.file)--.."\n\n"..binary.." "..t.file.."\n")
	local res = run_test(t)
	if(res) then
		--print("--- "..green.." PASSED\n"..reset)
		print(green.." [✓] "..reset)
		pass = pass + 1	
	else
		print("--- "..red.."FAILED\n"..reset)
		fail = fail + 1	
	end
end

if(test_count>rtest_count) then
	print(red.."missing "..test_count - rtest_count.." test(s) (from ./test/), missing:")
	for _,t in pairs(filest) do
		print("		--"..t)
	end
end
if(fail==0) then
	print(green.."all "..pass.." test(s) passed, all good:3"..reset)
else
	print(red.."passed "..pass.." failed "..fail..reset)
end
