#include <iostream>
#include "pretty.hh"

void _p_log(std::string s, int l, std::string f){
	std::cout<<color_gray<<"[ log ] "<<f<<":"<<l<<" "<<s<<color_reset<<std::endl;
}
void _p_warn(std::string s, int l, std::string f){
	std::cout<<color_lyellow<<"[ wrn ] "<<f<<":"<<l<<" "<<s<<color_reset<<std::endl;
}
void _p_err(std::string s, int l, std::string f){
	std::cout<<color_red<<"[ err ] "<<color_gray<<f<<":"<<l<<" "<<s<<color_reset<<std::endl;
}
void _p_ferr(std::string s,int i, int l, std::string f){
	std::cout<<color_red<<"[!err ] "<<color_red<<f<<":"<<l<<" "<<s<<color_reset<<std::endl;
	exit(i);
}
