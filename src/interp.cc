#include "interp.hh"

std::any single_ex(lexi l, state*s){
	//std::cout<<l.format(0)<<std::endl;
	if(!builtins.count(l.oper)&&l.oper!="__anon"){
		if(!s->functions.count(l.oper))	
			p_ferr("undefined refrence to "+l.oper+" on line:"+std::to_string(l.line)+":"+std::to_string(l._char),1);
		lexi gen = s->gen_args(s->functions[l.oper].argst,l);
		return s->functions[l.oper].exec(gen,s);
	}	

	int sel_args = builtins[l.oper]->check_args(l,s);
	
	if(sel_args==-1)
		p_ferr("incorrect usage on "+l.oper,1);
	lexi gen = s->gen_args(builtins[l.oper]->args[sel_args],l);
	return builtins[l.oper]->exec(&gen,s);
}

void interp(std::vector<lexi> inp, state* s){
	//std::cout<<batch_format(inp, 0);
	//std::cout<<inp[0].args[0].value.index();	
	//return;
	for(lexi l : inp){
		single_ex(l,s);
		//std::cout<<batch_format(inp, 0);
		//std::cout<<s->names[l.args[0].ident].ident;
	}
	
}
