#include "state.hh"
#include "cast.hh"

object dtype(object l, std::any a, type t, type wants){
	switch(t){
		case INT:
			l.value = std::any_cast<int>(a);
			l.type = INT;
			break;
		case DOUBLE:
		case STATEMENTC:	
		case NUMBER:
		case STRING:
		case CHAR:
		case NONE:
		case STATEMENT:
		case STATEMENT_UNEX:
		case REF:
		case ANON:
		case MAP:
		case ARRAY:
			break;
		case ANY:
			//std::vector<object> bb= std::any_cast<std::vector<object>>(a);
			
			object bb = std::any_cast<object>(a);
			l = bb;
			//std::cout<<bb;
			//p_warn("AAAA");
		break;
	}
	return l;
}

std::any func::exec(lexi l, state*s) {
	//return 0
	//if(args.size()>=2&&l.args.size()!=args[1].size()) abort();//exit(0);
	//std::cout<<l.args.size()<<" "<<args[1].size()<<std::endl;
	//for(int i = 0; i!=l.args.size();i++)
	//	std::cout<<l.args[i]<<std::endl;
	//std::cout<<args.size();
	state os = *s;
	if(1||args.size()==3){
		for(int i = 0; i!=l.args.size(); i++)
			s->names[args[i]] = l.args[i];
	}
	s->_return = object("","",STRING);
	for(lexi ss : calls){
		if(s->_break==BREAK)break;
		single_ex(ss,s);
	}
	s->_break = NO_FN;
	auto _return = s->_return;
	*s = os;
	return _return;//single_ex(l,s);
};

lexi state::gen_args(std::vector<type> b, lexi i){
	//if(b[0]==ANY) return i;
	if(i.oper=="__anon")return i;
	for(int x = 0; x!=i.args.size();x++){
		//std::cout<<"AA"<<std::endl;
		if(i.args[x].type==NONE&&b[x]!=NONE){	
			if(!names.count(i.args[x].ident)) continue;//p_ferr("undefined refrence to "+i.args[x].ident,1);
			if(names[i.args[x].ident].type==b[x])
				i.args[x] = names[i.args[x].ident];
			else {
				i.args[x] = cast_o(names[i.args[x].ident],b[x],this);
			}	
		} else if(b[x]!=ANON&&i.args[x].type==STATEMENT){
			//std::cout<<this->functions[i.args[x].n_value[0].oper]._return<<" "<<i.args.size()<<" "<<i.args[x].n_value.size();
			if(builtins.count(i.args[x].n_value[0].oper))
				i.args[x] = dtype(i.args[x],single_ex(i.args[x].n_value[0],this),builtins[i.args[x].n_value[0].oper]->_return,b[x]);
			else if(this->functions.count(i.args[x].n_value[0].oper))
				i.args[x] = dtype(i.args[x],single_ex(i.args[x].n_value[0],this),this->functions[i.args[x].n_value[0].oper]._return,b[x]);;
			if(i.args[x].type!=b[x])
				i.args[x] = cast_o(i.args[x],b[x],this);
		} else if(b[x]!=ANY&&b[x]!=i.args[x].type){
			i.args[x] = cast_o(i.args[x],b[x],this);
		}
	}
	return i;
}

void tttest(){

}
