#include <iostream>
#include <string>

#include "test.hh"

int main(int argc, char* argv[]){
	//std::cout<<batch_format(test("(exit 5 22)(meow \"wharrrr\" (a 5 (aaa 5.5) ) 445)"),0);
	//std::cout<<batch_format(test(f_read("./test/if.ll")),0);
	state state;
	state.names = {{"true",object("true",1,INT)},{"true",object("false",0,INT)},
									{"maybe",object("maybe",1.5,DOUBLE)}};
	interp(test(f_read(argv[1])),&state);
	//std::cout<<batch_format(test(f_read(argv[1])),0);
	//interp(test(f_read("./test/exit.ll")));
	return 0;
}
