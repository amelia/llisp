#define __parser_hh
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <variant>
#include <any>
//#include "state.hh"


#define INDENT_ITER 5

enum s_symbols {
	P_OPENING = '(',
	P_CLOSING = ')',
};
enum type {
	NONE, INT, DOUBLE, CHAR, STRING, STATEMENT_UNEX, STATEMENT, REF, ARRAY, ANON, ANY, NUMBER, STATEMENTC, MAP
};
enum reading_symbol {
	LOOKING, READING_O, READING_A, READING_CON
};

class lexi;
class object;

typedef std::variant<int, double, char, std::string, bool, long, std::vector<object>, std::map<std::string,object>> val;
class object {
	public:
		std::string ident = "";
		val value = NULL;
		std::vector<lexi> n_value;
		type type = NONE;
		object(){};
		object(std::string ident,val value, enum type type){
			this->ident = ident;
			this->value = value;
			this->type = type;
		}
		void print(){};
		std::vector<lexi> to_larray();
		void gen_value();
		void clear();
};
class lexi {
	public:
		int _char;
		int line;
		std::string oper;
		std::vector<object> args;
		std::string format(int);
			
		void clear();
};

//static std::map<type, std::any> typemap = {
//	{NONE,NULL}, {INT,(int)1}, {DOUBLE,(double)5.5}, {CHAR,(char)'5'},
//	{STRING,(std::string)"a"},{STATEMENT,new lexi}};

std::string batch_format(std::vector<lexi>, int);
std::vector<lexi> test(std::string);

