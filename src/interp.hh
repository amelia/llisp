#define __interp_hh
#include "parser.hh"
#include <vector>
#include <iostream>
#include <any>

#ifndef __pretty_hh
#include "pretty.hh"
#endif

#ifndef __builtins_hh
#include "builtins.hh"
#endif

#ifndef __state_hh
#include "state.hh"
#endif

void interp(std::vector<lexi>, state*);
std::any single_ex(lexi, state*);

