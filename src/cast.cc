#include "cast.hh"
#include "pretty.hh"

object cast_o(object i, type t, state* s){
	std::string out;
	std::vector<object> oba;
	if(t==i.type) return i;
	if(i.type==STATEMENT&&t!=STATEMENT&&t!=ANON&&i.ident!="__anon"){
		i = dtype(i,single_ex(i.n_value[0],s),builtins[i.n_value[0].oper]->_return,t);
		//i.type = STATEMENTC;
		if(i.type!=t)
			i = cast_o(i,t,s);
		return i;
	}
	switch(t){
		case INT:
			if(i.type==DOUBLE){
				i.type = INT;
				i.value = (int)std::get<double>(i.value);
				break;
			}
			if(i.type==STRING){
				i.type = INT;
				i.value = std::stoi(std::get<std::string>(i.value));
				break;
			}
			p_ferr("no avaliable converstion from "+std::to_string(i.type)+" to "+std::to_string(t),1);
			break;
		case NONE:
		case CHAR:
			break;
		case STRING:
			if(i.type==DOUBLE){
				i.type = STRING;
				i.value = std::to_string(std::get<double>(i.value));
				break;	
			}
			if(i.type==INT){
				i.type = STRING;
				i.value = std::to_string(std::get<int>(i.value));
				break;	
			}
			if(i.type==ARRAY){
				i.type = STRING;
				//out = "{ ";
				for(object o : std::get<std::vector<object>>(i.value)){
					out += std::get<std::string>(cast_o(o,STRING,s).value);
				}
				//if(out!="{ "){
				//	out[out.size()-1] = ' ';
				//	out[out.size()-2] = ' ';
				//}
				//out+="}";
				i.value = out;
				break;
			}
			p_ferr("no avaliable converstion from "+std::to_string(i.type)+" to "+std::to_string(t),1);
			break;
		case STATEMENT:
			
			break;	
		case STATEMENT_UNEX:
		case STATEMENTC:
		case DOUBLE:
		case REF:
			break;
		case ARRAY:
			if(i.type==STRING){
				i.type=ARRAY;
				for(char c : std::get<std::string>(i.value)){
					std::string a(1,c);
					oba.push_back(object(a,a,STRING));
				}
				i.value = oba;
				break;
			}
			p_ferr("no avaliable converstion from "+std::to_string(i.type)+" to "+std::to_string(t),1);
			break;
		case ANON:
		case ANY:
		case NUMBER:
		case MAP:	
			break;
	}
	return i;
}
