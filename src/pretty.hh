#include <string>

#define __pretty_hh

#define color_black "\e[30m"
#define color_red "\e[31m"
#define color_green "\e[32m"
#define color_yellow "\e[33m"
#define color_blue "\e[34m"
#define color_magenta "\e[35m"
#define color_cyan "\e[36m"
#define color_lgray "\e[37m"
#define color_gray "\e[90m"
#define color_lred "\e[91m"
#define color_lgreen "\e[92m"
#define color_lyellow "\e[93m"
#define color_lblue "\e[94m"
#define color_lmagenta "\e[95m"
#define color_lcyan "\e[96m"
#define color_white "\e[97m"
#define color_reset "\e[0m"

#define p_log(X) _p_log(X, __LINE__, __FILE__);
void _p_log(std::string, int, std::string);

#define p_warn(X) _p_warn(X, __LINE__, __FILE__);
void _p_warn(std::string, int, std::string);

#define p_err(X) _p_err(X, __LINE__, __FILE__);
void _p_err(std::string, int, std::string);

#define p_ferr(X,Y) _p_ferr(X, Y, __LINE__, __FILE__);
void _p_ferr(std::string, int, int, std::string);


