#define __builtins_hh


#include <typeinfo>
#include <iostream>
#include <map>
#include <typeindex>
#include <any>
#include <functional>

namespace builtin{
	class builtin;
}
#ifndef __interp_hh
#include "interp.hh"
#endif

#ifndef __state_hh
#include "state.hh"
#endif

//#ifndef __builtins_hh
//#define __builtins_hh

namespace builtin {
	
	class builtin {
	public:
		std::string call = "";
		std::vector<std::vector<type>> args = {};
		type _return = NONE;	
		std::any(*exec)(lexi*, state*);

		int check_args(lexi, state*);

		builtin(std::string _call, std::vector<std::vector<type>> _args, type __return, std::any(*e)(lexi*, state*)){
			call = _call;
			args = _args;
			_return = __return;
			exec = e;
		}
	};

	std::any a_exit(lexi*, state*);
	std::any write(lexi*, state*);
	std::any _if(lexi*, state*);
	std::any __anon(lexi*, state*);
	std::any _def(lexi*, state*);
	std::any _add(lexi*, state*);
	std::any _sub(lexi*, state*);
	std::any _mul(lexi*, state*);
	std::any _div(lexi*, state*);
	std::any _mod(lexi*, state*);
	std::any _get(lexi*, state*);
	std::any _geta(lexi*, state*);
	std::any _set(lexi*, state*);

	std::any _equ(lexi*, state*);
	std::any _great(lexi*, state*);
	std::any _less(lexi*, state*);
	std::any _great_o_equ(lexi*, state*);
	std::any _less_o_equ(lexi*, state*);
	std::any _nequ(lexi*, state*);
	std::any _and(lexi*, state*);
	std::any _not(lexi*, state*);
	std::any _or(lexi*, state*);
	std::any _xor(lexi*, state*);

	std::any _defn(lexi*, state*);
	std::any _udef(lexi*, state*);
	std::any _seta(lexi*, state*);
	std::any _type(lexi*, state*);	
	std::any _read(lexi*, state*);	

	//shorthands
	std::any _pp(lexi*, state*);
	std::any _mm(lexi*, state*);

	//array operations
	std::any _push(lexi*, state*);	
	std::any _pop(lexi*, state*);	
	std::any _insert(lexi*, state*);	
	std::any _rem(lexi*, state*);	
	std::any _len(lexi*, state*);	
	std::any _tail(lexi*, state*);

	std::any _return(lexi*, state*);

}


static builtin::builtin _write("write",{{STRING}},NONE,builtin::write);
static builtin::builtin _exit("exit",{{INT}},NONE,builtin::a_exit);
static builtin::builtin _if("if",{{INT,ANON},{INT,ANON,ANON}},NONE,builtin::_if);
static builtin::builtin __anon("if",{{ANY}},NONE,builtin::__anon);
static builtin::builtin _def("def",{{NONE,ANY}},NONE,builtin::_def);
static builtin::builtin _add("+",{{INT,INT}},INT,builtin::_add);
static builtin::builtin _sub("-",{{INT,INT}},INT,builtin::_sub);
static builtin::builtin _mul("*",{{INT,INT}},INT,builtin::_mul);
static builtin::builtin _div("/",{{INT,INT}},INT,builtin::_div);
static builtin::builtin _mod("%",{{INT,INT}},INT,builtin::_mod);
static builtin::builtin _get("get",{{ARRAY,INT}},ANY,builtin::_get);
static builtin::builtin _geta("geta",{{MAP,STRING}},ANY,builtin::_geta);
static builtin::builtin _set("set",{{NONE,INT,ANY}},NONE,builtin::_set);

static builtin::builtin _equ("==",{{INT,INT}},INT,builtin::_equ);
static builtin::builtin _great(">",{{INT,INT}},INT,builtin::_great);
static builtin::builtin _less("<",{{INT,INT}},INT,builtin::_less);
static builtin::builtin _great_o_equ(">=",{{INT,INT}},INT,builtin::_great_o_equ);
static builtin::builtin _less_o_equ("<=",{{INT,INT}},INT,builtin::_less_o_equ);
static builtin::builtin _nequ("!=",{{INT,INT}},INT,builtin::_nequ);
static builtin::builtin _and("&&",{{INT,INT}},INT,builtin::_and);
static builtin::builtin _not("!",{{INT}},INT,builtin::_not);
static builtin::builtin _or("||",{{INT,INT}},INT,builtin::_or);
static builtin::builtin _xor("^",{{INT,INT}},INT,builtin::_xor);

static builtin::builtin _defn("defn",{{NONE,ANON},{NONE,ARRAY,ANON}},ANY,builtin::_defn);
static builtin::builtin _udef("udef",{{NONE}},NONE,builtin::_udef);
static builtin::builtin _seta("seta",{{NONE,STRING,ANY}},NONE,builtin::_seta);
static builtin::builtin _type("type",{{ANY}},INT,builtin::_type);
static builtin::builtin _read("read",{{NONE}},NONE,builtin::_read);

static builtin::builtin _pp("++",{{NONE}},NONE,builtin::_pp);
static builtin::builtin _mm("--",{{NONE}},INT,builtin::_add);

static builtin::builtin _push("push",{{ARRAY,ANY}},ANY,builtin::_push); //WARN: all of these SHOULD return an array, figure it out
static builtin::builtin _pop("seta",{{ARRAY}},ANY,builtin::_pop);
static builtin::builtin _insert("insert",{{ARRAY,INT,ANY}},ANY,builtin::_insert);
static builtin::builtin _rem("rem",{{ARRAY, INT}},ANY,builtin::_rem); //WARN: up to here

static builtin::builtin _len("len",{{ARRAY}},INT,builtin::_len);
static builtin::builtin _tail("tail",{{ARRAY}},ANY,builtin::_tail);

static builtin::builtin _return("return",{{ANY},{}},NONE,builtin::_return);

static std::unordered_map<std::string, builtin::builtin*> builtins = {
	{"write",&_write},{"exit",&_exit},{"if",&_if},{"__anon",&__anon},{"def",&_def},{"+",&_add}
	,{"-",&_sub},{"*",&_mul},{"/",&_div},{"%",&_mod},{"get",&_get},{"geta",&_geta},{"set",&_set},{"==",&_equ},
	{">",&_great},{"<",&_less},{">=",&_great_o_equ},{"<=",&_less_o_equ},{"!=",&_nequ},{"&&",&_and},
	{"!",&_not},{"||",&_or},{"^",&_xor},{"defn",&_defn},{"udef",&_udef},{"seta",&_seta},{"++",&_pp},{"--",&_mm}
	,{"type",&_type},{"read",&_read},{"push",&_push},{"pop",&_pop},{"insert",&_insert},{"rem",&_rem},{"len",&_len}
	,{"tail",&_tail},{"return",&_return}
};

void ttest(lexi);

//#endif
