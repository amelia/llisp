#define __state_hh

#include <iostream>
#include <map>

class state;
void tttest();

#ifndef __builtins_hh
#include "builtins.hh"
#endif

#ifndef __parser_hh
#include "parser.hh"
#endif
object dtype(object l, std::any a, type t, type wants);
class func {
	public:
		std::string call = "";
		std::vector<std::string> args = {};
		std::vector<type> argst = {};

		type _return = NONE;
		std::vector<lexi> calls;
		std::any exec(lexi l, state*s);
		/*func(std::string _call, std::vector<type> _args, type __return, std::vector<lexi>e){
			call = _call;
			args = _args;
			_return = __return;
			calls = e;
		}*/

};

enum st {
	CONTINUE,BREAK,NO_FN
};

class state {
	public:
		st _break;
		std::any _return;
		std::map<std::string, object> names;
		std::map<std::string, func> functions;		
		lexi gen_args(std::vector<type>, lexi);
};


