#include "parser.hh"
#include "pretty.hh"

std::map<char, char> matching = {{'"','"'},{'(',')'},{'{','}'}};
std::vector<char> matching_keys = {'"','(','{'};
std::vector<char> matching_keys_end = {'"',')','}'};
std::map<char, type> matching_types = {{'"',STRING},{'(',STATEMENT_UNEX},{'{',ARRAY}};

void object::clear(){
	ident="";
	value = NULL;
	type = NONE;
}

std::vector<lexi> object::to_larray(){
	std::vector<lexi> r;
	if(n_value[0].oper!="__anon") return n_value;
	for(object ii : n_value[0].args){
		for(lexi s : ii.n_value){
			r.push_back(s);
		}
	}
	return r;
}

std::vector<std::string> split_excl(std::string in){
	std::string cur;
	std::vector<std::string> all;
	reading_symbol read = READING_A;
	int con_level;
	char last_con;

	for(int i = 0; i!=in.size(); i++){
		if(read==READING_A){
			if(std::find(matching_keys.begin(),matching_keys.end(),in[i]) != matching_keys.end()){
				con_level = 0;
				last_con = in[i];
				read = READING_CON;
				cur += in[i];
				continue;
			} else if(in[i]==','){
				all.push_back(cur);
				cur = "";
			} else 
				cur+=in[i];
		}else if(read==READING_CON){
					if(in[i]==matching[last_con]&&con_level==0){		
						cur += in[i];
						read = READING_A;
					} else {
						if(in[i]==last_con)
							con_level++;
						else if(in[i]==matching[last_con])
							con_level--;
						cur+=in[i];
			
			}
		}
	}
	if(cur!="")
		all.push_back(cur);

	//for(auto s : all)
	//	std::cout<<"'"<<s<<"'"<<std::endl;
	return all;
}

char array_break = ',';
char map_seper = ':';
void object::gen_value(){
	reading_symbol read = READING_O;
	int con_level;
	char last_con;
	
	//for ARRAY
	std::vector<object> array;
	object t;	
	std::vector<std::string> a;
	//for MAP
	std::map<std::string,object> map;
	std::string key;
	object mvalue;
	int layer = 0;
	int keyl;
	char last_open;
	switch(type){
		case ARRAY:	
			a = split_excl(ident);
			t.type = NONE;
			
			for(int i = 0; i!=a.size(); i++){
				if(a[i]=="")
					continue;
				t.ident = a[i];
				layer = 0;
				last_open = '\0';
				for(int z = 0; z!=a[i].size(); z++){
					if(z==0&&isdigit(a[i][z])){
						t.type = INT;
					}
					if(a[i][z]==':'&&layer==0)
						type = MAP;//std::cout<<"MAP";
					
					if(std::find(matching_keys.begin(),matching_keys.end(),a[i][z]) != matching_keys.end()&&last_open!=a[i][z]){
						layer++;
						last_open=a[i][z];
					} else if(std::find(matching_keys_end.begin(),matching_keys_end.end(),a[i][z]) != matching_keys_end.end()){
						layer--;
					}
				}

				for(int z = 0; z!=a[i].size()&&type!=MAP; z++){
					if(z==0&&std::find(matching_keys.begin(),matching_keys.end(),a[i][z]) != matching_keys.end()){
						t.type = matching_types[a[i][z]];
						t.ident = t.ident.substr(1,t.ident.size()-2);
					}
				}

				array.push_back(t);
				t.clear();
			}
			for(int i = 0; i!=array.size(); i++){
				if(array[i].type==STATEMENT_UNEX||array[i].type==ANON){
					array[i].ident = "("+array[i].ident+")";
					array[i].n_value = test(array[i].ident);
					array[i].type = STATEMENT;
				}
				array[i].gen_value();
			}
			
			value = array;
			if(type!=MAP)
				break;
		case MAP:
			array = std::get<std::vector<object>>(value);
			for(object b : array){
				//std::cout<<b.ident<<std::endl;

				read = READING_O;
				reading_symbol read2 = READING_O;
				t.clear();
				layer = 0;
				last_open = '\0';
				key = "";
				keyl = 0;
				for(int i = 0; i != b.ident.size(); i++){
					keyl++;
					
					if(read==READING_O&&std::find(matching_keys.begin(),matching_keys.end(),b.ident[i]) != matching_keys.end()){
						if(read2==READING_A)
							t.type = matching_types[b.ident[i]];
						read = READING_CON;
						last_open = b.ident[i];	
					} else if(read==READING_O&&b.ident[i]==':'){
						if(read2==READING_A)
							p_ferr("unexpcted :",1);
						read2 = READING_A;
					} else if(read2==READING_A&&read==READING_O){
						if(t.ident==""&&isdigit(b.ident[i])) t.type = INT;
						t.ident+=b.ident[i];
					} else if(read==READING_CON){
						if(b.ident[i]==matching[last_open]&&layer==0){
							read = READING_O;
						} else {
							if(b.ident[i]==last_open)
								layer++;
							else if(b.ident[i]==matching[last_open])
								layer--;
							if(read2==READING_O){	
								key+=b.ident[i];
								//std::cout<<key<<std::endl;
							}
							else
								t.ident+=b.ident[i];
			
						}
					}
					

					//t.ident+=b.ident[i];
				}
				//std::cout<<key<<" | "<<t.ident<<":"<<t.type<<std::endl;
				t.gen_value();
				map.insert({key,t});
			}
			value = map;
			//exit(0);
			break;
		case INT:
			value = std::stoi(ident);
			break;
		case DOUBLE:
			value = std::stod(ident);
			break;
		case CHAR:
			value = ident[0];
			break;
		case NONE:
		case STRING:
			value = ident;
		case STATEMENT:
		case STATEMENTC:
		case STATEMENT_UNEX:
		case REF:
		case NUMBER:
		case ANON:
			break;
		
		case ANY:
			break;
	}
}

std::string lexi::format(int indent){
	indent+=INDENT_ITER;
	std::string indent_s = "";
	for(int i = 0; i<indent;i++){
		indent_s+=" ";
	}
	std::string argv = "[ ";
	for(object a : args){
		if(a.type!=STATEMENT&&a.type!=ARRAY&&a.type!=MAP)
			argv+="'"+a.ident+"'"+":"+std::to_string(a.type)+", ";
		else if(a.type==ARRAY){
			argv+="\n"+indent_s+"{";
			for(object b : std::get<std::vector<object>>(a.value)){
				argv+="'"+b.ident+"'"+":"+std::to_string(b.type)+", ";	
			}
			argv+="}\n, ";
		} else if(a.type==MAP){
			std::cout<<"aaa";
		} else {
			argv+="\n"+indent_s+batch_format(a.n_value, indent+INDENT_ITER)+indent_s+":"+std::to_string(a.type)+",";
			//std::cout<<a.n_value.size();	
		}
			//argv+="[object]:"+std::to_string(a.type)+", ";
	}
	argv[argv.size()-1] = ' ';
	argv[argv.size()-2] = ' ';

	argv+="]";
	return "call: '"+oper+"' | argv: "+argv;
}
void lexi::clear(){
	oper = "";
	args = {};
}

std::string batch_format(std::vector<lexi> a,int indent){
	std::string r = "";
	for(lexi b : a){
		r+=b.format(indent)+"\n";
	}
	return r;
}
std::vector<lexi> test(std::string input){
	int open = 0;
	reading_symbol read = LOOKING;
	lexi tob;
	char last_con;
	int con_level;
	std::vector<lexi> all;
	object carg;
	tob.line = 0;
	tob._char = 0;
	for(int i = 0; i!=input.size(); i++){
		tob._char++;	
		if(input[i]=='\n') tob.line++;
		if(input[i]=='('){
			open++;
			if(read==LOOKING){
				read = READING_O;
				continue;
			}
		}
		else if(input[i]==')'){
			open--;
			if(open==0){
				if(read==READING_A||read==READING_CON){
					if(carg.ident!=""){
						//std::cout<<carg.ident<<std::endl;
						tob.args.push_back(carg);
					}
						
				}
				carg.clear();
				all.push_back(tob);
				tob.clear();
				read = LOOKING;
			}	
		}
		if(read==READING_O){
			if(input[i]==' ')
				read=READING_A;
			else if(tob.oper==""&&input[i]=='('){
				tob.oper="__anon";
				read = READING_CON;
				carg.clear();
				carg.type = ANON;
				con_level = 0;
				last_con = input[i];
			}
			else if(input[i]!=' '&&input[i]!='\n')
				tob.oper+=input[i];
		} else if(read==READING_A){
			if(input[i]==' '&&carg.ident!=""&&carg.ident!="\n"){
				tob.args.push_back(carg);
				carg.clear();
			} else if(std::find(matching_keys.begin(),matching_keys.end(),input[i]) != matching_keys.end()){
				con_level = 0;
				carg.clear();
				last_con = input[i];
				carg.type = matching_types[last_con];
				if(tob.oper=="__anon")
					carg.type = ANON;
				read = READING_CON;
			}
			else if(input[i]!=' '){

				carg.ident+=input[i];
				if(carg.type==NONE){
					if(isdigit(input[i]))
						carg.type=INT;//do other types:3
				}	else if(carg.type==INT&&input[i]=='.'){
					carg.type=DOUBLE;	
				}
			}
		} else if(read==READING_CON){
			if(input[i]==matching[last_con]&&con_level==0){
				tob.args.push_back(carg);
				carg.clear();
				read = READING_A;
			} else {
				if(input[i]==last_con)
					con_level++;
				else if(input[i]==matching[last_con])
					con_level--;
				carg.ident+=input[i];
			
			}

		}
	}
	for(int i = 0; i!=all.size(); i++){
		for(int y = 0; y!=all[i].args.size(); y++){
			if(all[i].args[y].type==STATEMENT_UNEX||all[i].args[y].type==ANON){
				all[i].args[y].ident = "("+all[i].args[y].ident+")";
				all[i].args[y].n_value = test(all[i].args[y].ident);
				all[i].args[y].type = STATEMENT;
			}
			all[i].args[y].gen_value();
		}
	}

	
	//std::cout<<tob.format()<<std::endl;
	return all;
}
