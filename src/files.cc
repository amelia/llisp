#include "files.hh"
#include "pretty.hh"

std::string f_read(std::string path){
	std::stringstream r;
	std::ifstream file(path);
	if(!file.is_open())
		p_ferr("file not found",1);
	r << file.rdbuf();
	return r.str();
}
