(defn recfib {n} (
(if (< n 0) (exit -9))
(if (== n 0) (return 0))
(if (|| (== n 1) (== n 2)) (return 1))
(return (+ (recfib (- n 1)) (recfib (- n 2))))
))
(write (recfib 9))
